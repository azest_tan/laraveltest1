<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flight;

class ShowNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $welcomestr = "Welcome You";

        $returnObj = [
            'wls'   => $welcomestr,
            'parm1' => 'pp33333p1',
            'parm2' => 'pp33333p2',
        ];
        
        $flightlist = Flight::all();

        // return $flightlist;

        return view('showname.index',[
            'wls' => $welcomestr,
            'parm1' => 'ppp1',
            'parm2' => 'ppp2',
            'aaa' => (object)$returnObj,
            'flightlist' => $flightlist
        ]);

        // {
        //   'wls':  welcomestr,
        //   'parm1': 'ppp1',
        //   'parm2': 'ppp2',
        //   'returnObj': returnObj,
        // }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('showname.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $flight = new Flight;

        $flight->fill($request->all());

        $flight->save();

        return $request->memo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $flight = Flight::find($id);
    
        return $flight;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
