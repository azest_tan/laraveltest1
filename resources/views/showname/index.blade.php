@extends('showname.layouts.main')

@section('title')
indexのタイトル
@endsection


@section('content')
{{-- {{$flightlist}} --}}

<table class="table">
    <thead>
        <tr>
            <th>id</th>
            <th>メモ</th>
            <th>作成日</th>
            <th>詳細</th>
        </tr>
    </thead>
    <tbody>
        @foreach($flightlist as $flight)
            <tr>
                <td scope="row">{{$flight->id}}</td>
                <td>{{$flight->memo}}</td>
                <td>{{$flight->created_at}}</td>
                <td>
                <a href="{{url('/showname/show/')}}/{{$flight->id}}">詳細</a>
                <td>
            </tr>
        @endforeach

    </tbody>
</table>

<hr>

<a name="" id="" class="btn btn-primary" href="{{url('/showname/create')}}" role="button">新規追加</a>

@endsection