@extends('showname.layouts.main')

@section('title')
新規作成ページ
@endsection


@section('content')

<form action="{{url('/showname/store')}}" method="post">
{{ csrf_field() }}

<div class="form-group">
  <label for=""></label>
  <input type="text" class="form-control" name="memo" id="memo" aria-describedby="helpId" placeholder="">
  <small id="helpId" class="form-text text-muted">メモを入力してください</small>
</div>

<input type="text" name="abc" id="abc3" value="I m abc dayo">


<button type="submit" class="btn btn-primary">新規追加</button>

</form>


@endsection