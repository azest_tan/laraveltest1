<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return view('hello');
});

// Route::get('/showname', function () {
//     return view('showname.index');
// });

// Route::post('/showname', function () {
//     return view('showname.finish');
// });

Route::get('/showname', 'ShowNameController@index');

Route::get('/showname/create', 'ShowNameController@create');

Route::post('/showname/store', 'ShowNameController@store');

Route::get('/showname/show/{id}', 'ShowNameController@show');